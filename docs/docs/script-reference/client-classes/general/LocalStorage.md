---
title: 'LocalStorage'
---
# `static class` LocalStorage <font size="4">(client-side)</font>

This class represents Local Storage mechanism.


## Properties
No properties.

----

## Methods
### key

This method gets the entry key via numerical index.

```cpp
any key(int index)
```

**Parameters:**

* `int` **index**: the index [from 0 to ...].
  
**Returns `any`:**

the entry key, or `null` if no key exists on passed numerical index.

----
### len

This method gets the number of entries in LocalStorage.

```cpp
any len()
```

  
**Returns `any`:**

the total number of entries.

----
### getItem

This method gets the specific entry value via string key.

```cpp
null|bool|int|float|string|array|table getItem(string key)
```

**Parameters:**

* `string` **key**: the entry key that was used to add specific entry.
  
**Returns `null|bool|int|float|string|array|table`:**

the valid entry value or `null` if entry doesn't exisits for specific string key.

----
### setItem

This method adds new entry or sets the existing one in LocalStorage.

```cpp
void setItem(string key, bool|int|float|string|array|table value)
```

**Parameters:**

* `string` **key**: the entry key.
* `bool|int|float|string|array|table` **value**: the entry value.
  

----
### removeItem

This method removes the exisitng entry via string key.

```cpp
void removeItem(string key)
```

**Parameters:**

* `string` **key**: the entry key.
  

----
### clear

This method removes all of the entries from LocalStorage.

```cpp
void clear()
```

  

----
