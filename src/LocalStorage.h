#ifndef _LOCAL_STORAGE_H
#define _LOCAL_STORAGE_H

#include <sqapi.h>

class LocalStorage
{
public:
	static bool init();

	static SQInteger sq_key(HSQUIRRELVM vm);
	static SQInteger len();
	static SQInteger sq_getItem(HSQUIRRELVM vm);
	static SQInteger sq_setItem(HSQUIRRELVM vm);
	static void removeItem(Sqrat::string key);
	static void clear();

	static void save();

private:
	static bool load();
};

#endif