#include <sqapi.h>
#include "LocalStorage.h"

static SQInteger sq_onExit(HSQUIRRELVM vm)
{
	LocalStorage::save();
	return 0;
}

static Sqrat::Function addEventHandler(const char* eventName, SQFUNCTION closure, int priority = 9999)
{
	using namespace SqModule;

	HSQOBJECT closureHandle;

	sq_newclosure(vm, closure, 0);
	sq_getstackobj(vm, -1, &closureHandle);

	Sqrat::Function func(vm, Sqrat::RootTable().GetObject(), closureHandle);
	Sqrat::RootTable().GetFunction("addEventHandler")(eventName, func, priority);

	sq_pop(vm, 1);

	return func;
}

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	// LocalStorage Init
	if (!LocalStorage::init())
		return SQ_ERROR;

	// Bind Event Handlers
	addEventHandler("onExit", sq_onExit, -1);

	/* squirreldoc (class)
	*
	* This class represents Local Storage mechanism.
	*
	* @static
	* @side		client
	* @name		LocalStorage
	*
	*/
	Sqrat::RootTable().Bind("LocalStorage", Sqrat::Table(vm)
		/* squirreldoc (method)
		*
		* This method gets the entry key via numerical index.
		*
		* @name			key
		* @param		(int) index the index [from 0 to ...].
		* @return		(string) the entry key, or `null` if no key exists on passed numerical index.
		*
		*/
		.SquirrelFunc("key", &LocalStorage::sq_key, 2, ".i")
		/* squirreldoc (method)
		*
		* This method gets the number of entries in LocalStorage.
		*
		* @name			len
		* @return		(int) the total number of entries.
		*
		*/
		.Func("len", &LocalStorage::len)
		/* squirreldoc (method)
		*
		* This method gets the specific entry value via string key.
		*
		* @name			getItem
		* @param		(string) key the entry key that was used to add specific entry.
		* @return		(null|bool|int|float|string|array|table) the valid entry value or `null` if entry doesn't exisits for specific string key.
		*
		*/
		.SquirrelFunc("getItem", &LocalStorage::sq_getItem, 2, ".s")
		/* squirreldoc (method)
		*
		* This method adds new entry or sets the existing one in LocalStorage.
		*
		* @name			setItem
		* @param		(string) key the entry key.
		* @param		(bool|int|float|string|array|table) value the entry value.
		*
		*/
		.SquirrelFunc("setItem", &LocalStorage::sq_setItem, 3, ".s.")
		/* squirreldoc (method)
		*
		* This method removes the exisitng entry via string key.
		*
		* @name			removeItem
		* @param		(string) key the entry key.
		*
		*/
		.Func("removeItem", &LocalStorage::removeItem)
		/* squirreldoc (method)
		*
		* This method removes all of the entries from LocalStorage.
		*
		* @name			clear
		*
		*/
		.Func("clear", &LocalStorage::clear)
	);

	return SQ_OK;
}
