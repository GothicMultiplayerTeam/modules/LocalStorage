#include "LocalStorage.h"
#include "g2o/registry.h"

#include <Windows.h>

#include <string>
#include <vector>
#include <fstream>
#include <mutex>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#define QUOTIFY(arg) #arg
#define STRINGIFY(arg) QUOTIFY(arg)

// 5MB
#define LOCAL_STORAGE_MAX_BYTES 5242880

// minutes * secondsPerMinute * milisecondsPerSecond
constexpr std::chrono::milliseconds LOCAL_STORAGE_AUTOSAVE_DELAY { 5 * 60 * 1000 };

static std::string saveDir;

static json jsonData({});
static std::vector<Sqrat::string> jsonKeys;

static bool dataModified = false;
static std::mutex mu;

static void set_json_object(json& jsonObject, Sqrat::Table& sqTable);
static void set_json_array(json& jsonArray, Sqrat::Array& sqArray);

static void set_sq_table(json& jsonObject, Sqrat::Table& sqTable);
static void set_sq_array(json& jsonArray, Sqrat::Array& sqArray);

void createDirectoryTree(const std::string& path)
{
	size_t pos = 0;
	do
	{
		pos = path.find_first_of("\\/", pos + 1);
		CreateDirectoryA(path.substr(0, pos).c_str(), NULL);
	} while (pos != std::string::npos);
}

bool LocalStorage::init()
{
	nlohmann::json_abi_v3_11_2::detail::use_ansi_encoding = true;
	saveDir = getStoreDir();
	
	if (saveDir.length() == 0)
	{
		SqModule::Error("failed reading store_dir from registry!");
		return false;
	}

	createDirectoryTree(saveDir);

	saveDir += "/LocalStorage.json";

	if (!load())
		return false;

	// if the file is empty, save {}
	if (jsonKeys.size() == 0)
	{
		dataModified = true;
		save();
	}

	// Spawning thread for auto-save which will be done periodically
	std::thread t([=]()
	{
		while (true)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(LOCAL_STORAGE_AUTOSAVE_DELAY));
			save();
		}
	});

	t.detach();
	return true;
}

SQInteger LocalStorage::sq_key(HSQUIRRELVM vm)
{
	SQInteger idx = 0;
	sq_getinteger(vm, 2, &idx);

	if (jsonKeys.size() <= idx)
		sq_pushnull(vm);
	else
		Sqrat::PushVar(vm, jsonKeys[idx]);

	return 1;
}

SQInteger LocalStorage::len()
{
	return jsonKeys.size();
}

SQInteger LocalStorage::sq_getItem(HSQUIRRELVM vm)
{
	const SQChar* key;
	sq_getstring(vm, 2, &key);

	if (!jsonData.contains(key))
	{
		sq_pushnull(vm);
		return 1;
	}

	auto& data = jsonData[key];

	switch (data.type())
	{
		default:
			sq_pushnull(vm);
			break;

		case json::value_t::boolean:
			Sqrat::PushVar(vm, data.get<bool>());
			break;

		case json::value_t::number_integer:
		case json::value_t::number_unsigned:
			Sqrat::PushVar(vm, data.get<SQInteger>());
			break;

		case json::value_t::number_float:
			Sqrat::PushVar(vm, data.get<SQFloat>());
			break;

		case json::value_t::string:
			Sqrat::PushVar(vm, data.get<Sqrat::string>());
			break;

		case json::value_t::array:
		{
			Sqrat::Array array(vm);
			set_sq_array(data, array);

			Sqrat::PushVar(vm, array);
			break;
		}

		case json::value_t::object:
		{
			Sqrat::Table table(vm);
			set_sq_table(data, table);

			Sqrat::PushVar(vm, table);
			break;
		}
	}

	return 1;
}

SQInteger LocalStorage::sq_setItem(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 3)
		return sq_throwerror(vm, "wrong number of parameters");

	std::lock_guard<std::mutex> lock(mu);

	const SQChar* key;
	sq_getstring(vm, 2, &key);

	HSQOBJECT value;
	sq_getstackobj(vm, 3, &value);

	switch (value._type)
	{
		case OT_BOOL:
		{
			SQBool value;
			sq_getbool(vm, 3, &value);

			jsonData[key] = bool(value != 0);
			break;
		}

		case OT_INTEGER:
		{
			SQInteger value;
			sq_getinteger(vm, 3, &value);

			jsonData[key] = value;
			break;
		}

		case OT_FLOAT:
		{
			SQFloat value;
			sq_getfloat(vm, 3, &value);

			jsonData[key] = value;
			break;
		}

		case OT_STRING:
		{
			const SQChar* value;
			sq_getstring(vm, 3, &value);

			jsonData[key] = Sqrat::string(value);
			break;
		}

		case OT_ARRAY:
		{
			Sqrat::Array sqArray(value);
			json jsonArray = json::array();

			set_json_array(jsonArray, sqArray);

			jsonData[key] = jsonArray;
			break;
		}

		case OT_TABLE:
		{
			Sqrat::Table sqObject(value);
			json jsonObject = json::object();

			set_json_object(jsonObject, sqObject);

			jsonData[key] = jsonObject;
			break;
		}

		default:
			return sq_throwerror(vm, "(LocalStorage::setItem) wrong type of parameter 2, expecting bool|int|float|string|array|table");
	}

	std::string dump = jsonData.dump(-1, 32, false, nlohmann::json_abi_v3_11_2::detail::error_handler_t::ignore);

	if (dump.size() > LOCAL_STORAGE_MAX_BYTES)
	{
		jsonData.erase(key);
		return sq_throwerror(vm, "(LocalStorage::setItem) storage size exceeded, the limit is " STRINGIFY(LOCAL_STORAGE_MAX_BYTES) " bytes.");
	}

	if (std::find(jsonKeys.begin(), jsonKeys.end(), key) == jsonKeys.end())
		jsonKeys.push_back(key);

	dataModified = true;
	return 0;
}

void LocalStorage::removeItem(Sqrat::string key)
{
	if (!jsonData.contains(key))
		return;

	std::lock_guard<std::mutex> lock(mu);

	auto it = std::find(jsonKeys.begin(), jsonKeys.end(), key);
	
	if (it != jsonKeys.end())
	{
		jsonData.erase(jsonData.find(key));
		jsonKeys.erase(it);

		dataModified = true;
	}
}

void LocalStorage::clear()
{
	std::lock_guard<std::mutex> lock(mu);

	jsonData.clear();
	jsonKeys.clear();

	dataModified = true;
}

void LocalStorage::save()
{
	if (!dataModified)
		return;

	std::lock_guard<std::mutex> lock(mu);

	std::ofstream file(saveDir);

	file << jsonData.dump(-1, 32, false, nlohmann::json_abi_v3_11_2::detail::error_handler_t::ignore) << std::flush;
	dataModified = false;

	file.close();
}

bool LocalStorage::load()
{
	std::ifstream file(saveDir);

	if (!file.is_open())
		return true; // ignore not existing file (maybe someone use module for the first time)

	file.seekg(0, file.end);
	auto fileSize = file.tellg();

	if (fileSize > LOCAL_STORAGE_MAX_BYTES)
	{
		file.close();

		SqModule::Print("Warning! localStorage.json is larger than " STRINGIFY(LOCAL_STORAGE_MAX_BYTES) " bytes, it's contents will be ignored.");
		return true;
	}
	else if (fileSize == 0)
	{
		file.close();
		return true; // ignore empty file
	}

	file.seekg(0, file.beg);

	try
	{
		file >> jsonData;
	}
	catch (json::exception e)
	{
		jsonData.clear();
		file.close();

		SqModule::Error("failed parsing localStorage.json ");
		SqModule::Error(e.what());

		return false;
	}

	auto it = jsonData.begin();

	while (it != jsonData.end())
	{
		if (!it.value().is_null())
		{
			jsonKeys.push_back(it.key());
			++it;
		}
		else
			it = jsonData.erase(it);
	}

	file.close();
	return true; // everything OK, load the module
}

static Sqrat::string tostring(HSQOBJECT obj)
{
	using namespace SqModule;

	sq_pushobject(vm, obj);

	if (SQ_FAILED(sq_tostring(vm, -1)))
	{
		sq_pop(vm, 1);
		return "";
	}

	const SQChar* cstring = nullptr;
	sq_getstring(vm, -1, &cstring);

	Sqrat::string result = (cstring != nullptr) ? cstring : "";

	sq_pop(vm, 2);
	return result;
}

static void set_json_object(json& jsonObject, Sqrat::Table& sqTable)
{
	Sqrat::Object::iterator it;

	while (sqTable.Next(it))
	{
		Sqrat::string index = tostring(it.getKey());
		if (index == "")
			return;

		HSQOBJECT val = it.getValue();

		switch (val._type)
		{
		case OT_NULL:
		{
			jsonObject[index] = nullptr;
			break;
		}

		case OT_BOOL:
		{
			jsonObject[index] = val._unVal.nInteger == 1;
			break;
		}

		case OT_INTEGER:
		{
			jsonObject[index] = val._unVal.nInteger;
			break;
		}

		case OT_FLOAT:
		{
			jsonObject[index] = val._unVal.fFloat;
			break;
		}

		case OT_STRING:
		{
			jsonObject[index] = sq_objtostring(&val);
			break;
		}

		case OT_ARRAY:
		{
			json nestedJsonArray = json::array();
			Sqrat::Array nestedSqArray(val);

			set_json_array(nestedJsonArray, nestedSqArray);
			jsonObject[index] = nestedJsonArray;
			break;
		}

		case OT_TABLE:
		{
			json nestedJsonObject = json::object();
			Sqrat::Table nestedSqTable(val);

			set_json_object(nestedJsonObject, nestedSqTable);
			jsonObject[index] = nestedJsonObject;
			break;
		}
		}
	}
}

static void set_json_array(json& jsonObject, Sqrat::Array& sqArray)
{
	Sqrat::Object::iterator it;

	while (sqArray.Next(it))
	{
		HSQOBJECT idx = it.getKey();
		HSQOBJECT val = it.getValue();

		SQInteger index = idx._unVal.nInteger;

		switch (val._type)
		{
		case OT_NULL:
		{
			jsonObject[index] = nullptr;
			break;
		}

		case OT_BOOL:
		{
			jsonObject[index] = val._unVal.nInteger == 1;
			break;
		}

		case OT_INTEGER:
		{
			jsonObject[index] = val._unVal.nInteger;
			break;
		}

		case OT_FLOAT:
		{
			jsonObject[index] = val._unVal.fFloat;
			break;
		}

		case OT_STRING:
		{
			jsonObject[index] = sq_objtostring(&val);
			break;
		}

		case OT_ARRAY:
		{
			json nestedJsonArray = json::array();
			Sqrat::Array nestedSqArray(val);

			set_json_array(nestedJsonArray, nestedSqArray);
			jsonObject[index] = nestedJsonArray;
			break;
		}

		case OT_TABLE:
		{
			json nestedJsonObject = json::object();
			Sqrat::Table nestedSqTable(val);

			set_json_object(nestedJsonObject, nestedSqTable);
			jsonObject[index] = nestedJsonObject;
			break;
		}
		}
	}
}

static void set_sq_array(json& jsonArray, Sqrat::Array& sqArray)
{
	using namespace SqModule;

	for (auto& value : jsonArray)
	{
		switch (value.type())
		{
		case json::value_t::null:
			sqArray.Append(nullptr);
			break;

		case json::value_t::boolean:
			sqArray.Append(value.get<bool>());
			break;

		case json::value_t::number_integer:
		case json::value_t::number_unsigned:
			sqArray.Append(value.get<SQInteger>());
			break;

		case json::value_t::number_float:
			sqArray.Append(value.get<SQFloat>());
			break;

		case json::value_t::string:
			sqArray.Append(value.get<Sqrat::string>());
			break;

		case json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			set_sq_array(value, nestedSqArray);
			sqArray.Append(nestedSqArray);
			break;
		}

		case json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			set_sq_table(value, nestedSqTable);
			sqArray.Append(nestedSqTable);
			break;
		}
		}
	}
}

static void set_sq_table(json& jsonObject, Sqrat::Table& sqTable)
{
	using namespace SqModule;

	for (auto& el : jsonObject.items())
	{
		auto& key = el.key();
		auto& value = el.value();

		switch (el.value().type())
		{
		case json::value_t::null:
			sqTable.SetValue(key.c_str(), nullptr);
			break;

		case json::value_t::boolean:
			sqTable.SetValue(key.c_str(), value.get<bool>());
			break;

		case json::value_t::number_integer:
		case json::value_t::number_unsigned:
			sqTable.SetValue(key.c_str(), value.get<SQInteger>());
			break;

		case json::value_t::number_float:
			sqTable.SetValue(key.c_str(), value.get<SQFloat>());
			break;

		case json::value_t::string:
			sqTable.SetValue(key.c_str(), value.get<Sqrat::string>());
			break;

		case json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			set_sq_array(value, nestedSqArray);
			sqTable.SetValue(key.c_str(), nestedSqArray);
			break;
		}

		case json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			set_sq_table(value, nestedSqTable);
			sqTable.SetValue(key.c_str(), nestedSqTable);
			break;
		}
		}
	}
}