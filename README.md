# Local Storage

## Introduction

This **client-side** module allows the server creators to utilize the LocalStorage API for serializing informations.  
The g2o platform doesn't support the file API from squirrel on client-side due to security reasons, so this project feels the gap  
of the missing feature. The API usage is very similar to the one from browser (the main inspiration came from it).  
The code doesn't give the user ability to save/load data, instead it exposes an API to get or set (create/update) values.  
The API itself will handle the saving data to a file `LocalStorage.json`, which will be located in this directory: `Game/Multiplayer/store/`

## Documentation

https://gothicmultiplayerteam.gitlab.io/modules/LocalStorage